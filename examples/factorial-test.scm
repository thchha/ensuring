;; execute by loading ../ensuring.scm as the module and then this file.

(test-begin! "Testing example.")
  ;; Load in the files to test against. Multiple can be supplied.

  ; Between some tests, execution will sleep for a second...
(test-against "files/factorial.scm")

;; Create a test with multiple asserts.
;; It will stop when a failed assert is encountered, but test-execution is proceeded.
(test "euler percision <- will fail at last assert."
      assert (equal? 9864101/3628800 (ensure 'euler 10))
      assert (equal? 2.718281828459045 (exact->inexact (ensure 'euler 100)))
      ;; fails:
      assert (equal? 161/60 (ensure 'euler 5)))

(test "mocking functions"
      assert (equal? 100 (ensure-with-mocks
                           ;; override or declare procedures (mocks)
                           '((euler (lambda (x) (factorial x)))
                             (factorial (lambda (x) (* x 1))))
                           'euler 100)))

;; Switch the context, drop other bindings.
;; Use `(ensure` for introspection of a method from the loaded file(s).
(test-against "files/lists.scm")

(test "ordering of addlist"
      (thread-sleep! 2)
      assert (equal? '(1 2 3 4 5) '(1 2 3 4 5))
      assert (equal? '(1 2 3 4 (5)) (ensure 'addlist '(1 2 3) 4 '(5)))
      assert (equal? '(4 5 1 2 3) (ensure 'addlist '(4 5) 1 2 3)))

; Writing within a test isn't properly supported, yet.

;; use your scheme implementation to leverage things outside R5RS.
(test "an error but handle it on the calling site."
      (thread-sleep! 3)
      assert (equal? "#<error-exception #2>" (with-exception-handler
                                               (lambda (ex) (object->string ex))
                                               (lambda () (error "some error")))))

;; finishs with a summary.
(test-summary!)
