(define text-black   30)
(define text-red     31)
(define text-green   32)
(define text-yellow  33)
(define text-blue    34)
(define text-magenta 35)
(define text-turkis  36)
(define text-white   37)

(define colored-display
  (lambda (color message)
    (let ((escape (string #\escape #\[ #\1 #\;))
          (end (string #\escape #\[ #\m)))
      (display
        (string-append
          escape (number->string (if (> color 0) color 0)) "m"
          message end)))))
