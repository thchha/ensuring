# An idea to enable a portable testing framework

*moved to a better name*: https://gitlab.com/thchha/ensure
Now _ARCHIVED_.

The idea is:
- Read in data from a file and interpret within a sealed scope.
  This will enable one to inspect dependencies within procedures.
- Each procedure can be tested in isolation.
- Only require a command line argument to invoke tests.

## limitations

Tell me by trying it.
