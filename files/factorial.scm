
(define variable 'this-should-be-ignored)

(define (factorial n)
  (if (< n 2) 1
      (* n (factorial (- n 1)))))

(define (euler n)
  (if (= n 0)
      1
      (+ (/ 1 (factorial n)) (euler (- n 1)))))

