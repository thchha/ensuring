;;   ___ _ __  ___ _   _ _ __ ___
;;  / _ \ '_ \/ __| | | | '__/ _ \
;; |  __/ | | \__ \ |_| | | |  __/
;;  \___|_| |_|___/\__,_|_|  \___|
;;
;; A > R5RS testing framework
;; Allows unit and integration tests with mocking capabilities.
;; PUBLIC DOMAIN, refer to LICENSE-file.
;; TODO: Supports source code locations.
;;       read the file two times:
;;         - as datums, only top-level
;;         - line wise with count.
;;       Then compare the datum to the file-content ...

;; This file enables ANSI escape sequences @see `Select Graphic Rendition`
;; FIXME: Get permission of ski and jcowan to name drop
(include "colors.scm")

;; defines mutable variables
;; @see `(test-against! files ...)` mutates
(define bindings 'uninitialized)
(define ensure-asserts-within-test-case 0)
(define ensure-executed-tests 0)
(define ensure-suceeded-tests 0)
(define ensure-failed-tests 0)

(define ensure-beeing-verbose? #t)
(define trace? #f)

(define test-begin!
  (lambda (name)
    (display "# ") (display name) (newline)
    (colored-display text-black "Ensuring is provided for the public domain. Refer to the LICENSE file.") (newline)
    (colored-display text-black "You can request help by the author at thomas . hage @ live . de") (newline)
    (colored-display text-black "Feel free to submit suggestions and issues as well.") (newline)
    (colored-display text-black "There is a dedicated repository for the development at:") (newline)
    (colored-display text-black "   https://gitlab.com/thchha/ensuring") (newline)))

; makes macro available to other files
(eval
  (quote
    (define-syntax test
      (syntax-rules (assert intern asserting increment-success! increment-fail!)
        ((_ increment-success!)
         (begin
           (set! ensure-suceeded-tests (+ ensure-suceeded-tests 1))
           (when ensure-beeing-verbose?
             (colored-display text-green " OK ")
             (newline))))
        ((_ increment-fail! number comparator? expected actual)
         (begin
           (set! ensure-failed-tests (+ ensure-failed-tests 1))
           (when ensure-beeing-verbose?
             (colored-display text-red "FAIL")
             (newline)
             (display "failed assert #") (display number)
             (display " with comparator: ") (display comparator?)
             (newline)
             (colored-display text-white "  expected: ") (pp expected)
             (colored-display text-red "  actual:   ") (pp actual))))
        ((_ asserting comparator? expected actual)
         (let ((res actual))
           (if (apply (eval comparator?) (list expected res))
               #t
               (list expected res))))
       ((_ intern evals assert (comparator? expected actual))
        (letrec ((ensure (lambda (x i)
                           (if (null? x)
                               (test increment-success!)
                               (let ((res (test asserting
                                               (caar x)
                                               (eval (cadar x))
                                               (eval (caddar x)))))
                                (if (eq? res #t)
                                    (ensure (cdr x) (+ i 1))
                                    (test increment-fail! i (caar x) (car res) (cadr res))))))))
          (set! ensure-asserts-within-test-case (+ 1 ensure-asserts-within-test-case))
            (if ensure-beeing-verbose?
                ; After writing out the test case, we can prepend the result
                ; by going to the start of the line and overwrite "Test".
                (display (string #\escape #\[ #\2 #\K #\return)))
            (ensure (reverse (cons `(comparator? expected actual) evals)) 1)))
       ((_ intern res assert (comparator? expected actual) body ...)
        (begin
          (set! ensure-asserts-within-test-case (+ 1 ensure-asserts-within-test-case))
          (test intern (cons `(comparator? expected actual) res) body ...)))
       ((_ intern res stmt body ...)
        (begin
          stmt
          (test intern res body ...)))
       ((_ test-title stmt body ...)
        (begin
          (set! ensure-executed-tests (+ 1 ensure-executed-tests))
          (set! ensure-asserts-within-test-case 0)
          (if ensure-beeing-verbose?
              (begin
                (display "Test #")
                (display ensure-executed-tests)
                (display " - ")
                (display test-title)))
          (test intern '() stmt body ...)))))))

;; This procedure should actually *bump-out* the bindings to the enclosing
;; lexical scope. Even though it is currently working as intended,
;; reading in the verify!s will require a mutable environment, so that
;; one can decrement the invocations. This _should_ be possible with
;; R5RS-only.
(define ensure-with-mocks
  (lambda (environment method . args)
    (letrec ((remove (lambda (set env)
                       (if (null? set)
                           env
                           (if (assoc (caar set) env)
                               (remove (cdr set) env)
                               (remove (cdr set) (cons (car set) env)))))))
      (if (and (pair? bindings)
               (symbol? method)
               (assoc method bindings))
          (eval `(letrec* ,(if (pair? environment)
                               (remove bindings environment)
                               bindings)
                   (if trace?
                       (begin
                         (newline)
                         (trace ,method)))
                   (apply ,method (quote ,args))))
          (fail! "No function named " method " prepared with (ensure-in <files>).")))))

(define ensure
  (lambda (method . args)
    (apply ensure-with-mocks '() method args)))

;; TODO: `(test-against *.files)` should inject quoting to read-in-macros,
;;       before executing - IF this is possible. I guess it should be.
(define-syntax trace-macro (syntax-rules () ((_ x) (quote x))))

;; preprocesses all files and extracts
;;  - `(define (...) ...)`
;;  - `(define _ (lambda (...) ...))`
;;  TODO: - `(define-syntax _ ...)`
;;
;  Puts all resolved targets into a variable to refer to within tests.
(define test-against
  (lambda files
    (set! bindings '())
    (for-each
      (lambda (file)
        (call-with-input-file
          file
          (lambda (p)
            (let ((transform-to-lambda?
                    (lambda (datum procs)
                      (if (and (eq? 'define (car datum))
                               (or (pair? (cadr datum)) ; syntax: (define (... => procedure
                                   (and ; syntax: (define _ (lambda ... => procedure
                                     (pair? (caddr datum))
                                     (eq? 'lambda (caaddr datum)))))
                          (cons
                            (if (pair? (cadr datum))
                                ; (define (name args) ... => name . (lambda (args) ...
                                (let ((proc `((lambda ,(cdadr datum) ,@(cddr datum)))))
                                  (cons (caadr datum) proc))
                                ; (define _ (lambda ... => 'name . (lambda ...
                                (cons (cadr datum) (cddr datum)))
                            procs)
                          procs))))
              (set! bindings
                (append
                  (let filter ((procs '()))
                    (let ((datum (read p)))
                      (if (eof-object? datum)
                          (reverse procs)
                          (filter (transform-to-lambda? datum procs)))))
                  bindings))))))
      files)
    (display "Loading files: ")
    (for-each (lambda (f)
                 (display f) (display " "))
              files)
    (newline)))

(define test-summary!
  (lambda ()
    (display "-----------------------") (newline)
    (display "   ")
    (cond
      ((= 0 ensure-executed-tests)
       (display "No tests executed.") (newline))
      ((= 0 ensure-failed-tests)
       (colored-display text-green (number->string ensure-suceeded-tests))
       (display " tests succeded.") (newline))
      (else
        (display ensure-executed-tests)
        (display " tests executed, but ")
        (colored-display text-red (number->string ensure-failed-tests))
        (display " failed.")
        (newline)))
    (exit ensure-failed-tests)))
